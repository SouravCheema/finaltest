//
//  GameScene.swift
//  SlimeZerkTest
//
//  Created by Parrot on 2019-02-25.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit



class GameScene: SKScene, SKPhysicsContactDelegate {
    // MARK: Variables for tracking time
    private var lastUpdateTime : TimeInterval = 0
    
    var lives : Int = 5
    var levelComplete: Bool = false
    var orange:Orange?
    var mouseStartingPosition:CGPoint = CGPoint(x:0, y:0)
    
    // MARK: Sprite variables
    var player:SKSpriteNode = SKSpriteNode(imageNamed: "player")
    var upButton:SKSpriteNode = SKSpriteNode(imageNamed: "upButton")
    var downButton:SKSpriteNode = SKSpriteNode(imageNamed: "downButton")
    var leftButton:SKSpriteNode = SKSpriteNode(imageNamed: "leftButton")
    var rightButton:SKSpriteNode = SKSpriteNode(imageNamed: "rightButton")
    var wall:SKSpriteNode = SKSpriteNode(imageNamed: "Wall")
    var enemy:SKSpriteNode = SKSpriteNode(imageNamed: "enemy")
    var exit:SKSpriteNode = SKSpriteNode()
    var backgroundMusic: SKAudioNode!
    // categorize the sprites
    
    // MARK: Label variables
    var livesLabel:SKLabelNode = SKLabelNode(text:"")
    
    
    // MARK: Scoring and Lives variables
    
    
    // MARK: Game state variables
    
      var gameInProgress = true
    let backgroundSound = SKAudioNode(fileNamed: "bg.mp3")
    // MARK: Default GameScene functions
    // -------------------------------------
    override func didMove(to view: SKView) {
        
        
        self.addChild(backgroundSound)
        self.lastUpdateTime = 0
        self.physicsWorld.contactDelegate = self
        // get sprites from Scene Editor
        self.player = self.childNode(withName: "player") as! SKSpriteNode
        self.upButton = self.childNode(withName: "upButton") as! SKSpriteNode
        self.downButton = self.childNode(withName: "downButton") as! SKSpriteNode
        self.leftButton = self.childNode(withName: "leftButton") as! SKSpriteNode
        self.rightButton = self.childNode(withName: "rightButton") as! SKSpriteNode
        self.exit = self.childNode(withName: "Exit") as! SKSpriteNode
        // get labels from Scene Editor
        self.livesLabel = self.childNode(withName: "livesLabel") as! SKLabelNode
        
        exit.physicsBody?.collisionBitMask = 0
        self.player.physicsBody?.collisionBitMask = 5
        
        self.player.physicsBody?.contactTestBitMask = 12
        
        
        
        
        let oneRevolution:SKAction = SKAction.rotate(byAngle: CGFloat.pi * 1, duration: 1)
        let repeatRotation:SKAction = SKAction.repeatForever(oneRevolution)
        
        player.run(repeatRotation)

    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        lives = lives - 1 ;
        self.livesLabel.text = "\(lives)"
        if(lives == 0) {
            self.playerLoses()
        }
        
        //self.nextLevel()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            // if the touch results in a null value, then exit
            return
        }
        
        let positionInScene = touch.location(in: self)
        let touchedNode = self.atPoint(positionInScene)
        
        if let name = touchedNode.name {
            if (name == "bButton") {
                //print("YOU CLICKED THE TREE")
                // add an orange where the person clicked
                self.orange = Orange()
                orange?.position = positionInScene
                orange?.name = "orange"
                addChild(self.orange!)
                self.orange?.physicsBody?.restitution = 1.0
                
                
                self.orange?.physicsBody?.isDynamic = false
                
                // set the starting position of the finger
                self.mouseStartingPosition = positionInScene
            }
            if(name == "Restart"){
                let message = SKLabelNode(text:"LEVEL COMPLETE")
                message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
                message.fontColor = UIColor.magenta
                message.fontSize = 100
                message.fontName = "AvenirNext-Bold"
                
                addChild(message)
                
//                guard let nextLevelScene = GameScene.nextLevel(levelNumber: s         elf.currentLevel) else {
//                    print("Error when loading next level")
//                    return
//                }
//
//                // wait 1 second then switch to next leevl
//                let waitAction = SKAction.wait(forDuration:1)
//                nextLevelScene.setLevel(levelNumber: self.currentLevel)
//                let showNextLevelAction = SKAction.run {
//                    let transition = SKTransition.flipVertical(withDuration: 2)
//                    nextLevelScene.scaleMode = .aspectFill
//                    self.scene?.view?.presentScene(nextLevelScene, transition:transition)
//                }
//
//                let sequence = SKAction.sequence([waitAction, showNextLevelAction])
//
//                self.run(sequence)
//            }
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            // if the touch results in a null value, then exit
            return
        }
        
        let positionInScene = touch.location(in: self)
        let touchedNode = self.atPoint(positionInScene)
        
    
        
        if let name = touchedNode.name {
           
            
            if(name == "musicButton") {
                   backgroundSound.run(SKAction.stop())
                
            }
            
            if (name == "upButton") {
                print("Player touched  up button")
                let playerY = self.player.position.y + 10;
                self.player.position = CGPoint(x: self.player.position.x, y: playerY)
            }
            else if(name == "downButton") {
                print("Player Touched Down Button")
                let playerYMinus = self.player.position.y - 10;
                self.player.position = CGPoint(x: self.player.position.x, y:playerYMinus )
            }
            else if(name == "leftButton") {
                print("player touched left button")
                let playerXMinus = self.player.position.x - 10;
                self.player.position = CGPoint(x: playerXMinus, y: self.player.position.y)
            }
            else if(name == "rightButton") {
                print("player touched right button")
                let playerX = self.player.position.x + 10;
                self.player.position = CGPoint(x: playerX, y: self.player.position.y)
            }
            
            
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
      
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        
        // Initialize _lastUpdateTime if it has not already been
        if (self.lastUpdateTime == 0) {
            self.lastUpdateTime = currentTime
        }

        // Calculate time since last update
        let dt = currentTime - self.lastUpdateTime
    
        // HINT: This code prints "Hello world" every 5 seconds
        if (dt > 5) {
            print("HELLO WORLD!")
            self.lastUpdateTime = currentTime
        }
        
    }
    
    func playerLoses() {
                self.gameInProgress = false
            
                let message = SKLabelNode(text:"YOU LOSE!")
                message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
                message.fontColor = UIColor.magenta
                message.fontSize = 100
                message.fontName = "AvenirNext-Bold"
        
                addChild(message)
            }
    
        func playerWins() {
                self.gameInProgress = false
            
                let message = SKLabelNode(text:"YOU WIN!")
                message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
                message.fontColor = UIColor.magenta
                message.fontSize = 100
                message.fontName = "AvenirNext-Bold"
        
                addChild(message)
        
        
            }

    
    // MARK: Custom GameScene Functions
    // Your custom functions can go here
    // -------------------------------------
    @objc func restartGame() {
        
        // load Level2.sks
        let scene = GameScene(fileNamed:"BerzerkLevel")
        scene?.scaleMode = scaleMode
        view!.presentScene(scene)
    }
    
    @objc func nextLevel() {
        let scene = GameScene(fileNamed: "BerzerkLevel2")
        scene?.scaleMode = scaleMode
        view!.presentScene(scene)
    }
    
    func gameOver() {
        self.levelComplete = true
        
        let message = SKLabelNode(text:"LEVEL COMPLETE!")
        message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
        message.fontColor = UIColor.magenta
        message.fontSize = 100
        message.fontName = "AvenirNext-Bold"
        
        addChild(message)
        
        // restart the game after 3 seconds
        perform(#selector(GameScene.restartGame), with: nil,
                afterDelay: 3)
        
    }
    
    
    
}

    
